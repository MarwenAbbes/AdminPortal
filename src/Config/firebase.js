import firebase from 'firebase/app'
import "firebase/firestore"
import 'firebase/auth'
import 'firebase/storage'


const firebaseConfig = {
    apiKey: "AIzaSyAo94RC9Vwa0znyHqucA0O7k4QSvXJUD90",

    authDomain: "daseducation-cc706.firebaseapp.com",
  
    projectId: "daseducation-cc706",
  
    storageBucket: "daseducation-cc706.appspot.com",
  
    messagingSenderId: "176773999099",
  
    appId: "1:176773999099:web:dc3c70dc55bc4b679d835e",
  
    measurementId: "G-0HPCBDMBC2"
  
}

firebase.initializeApp(firebaseConfig)
export const auth = firebase.auth();
export const db =firebase.firestore();
// export  const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
// export const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();


export  default firebase
