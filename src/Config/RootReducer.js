import {combineReducers,createStore} from 'redux'
import userReducer from './reducer'
import { firebaseReducer } from 'react-redux-firebase'
import firebase from 'firebase'

const rootReducer = combineReducers({
    firebase : firebaseReducer,
    user : userReducer,
});

const initialState = {}
const store = createStore(rootReducer, initialState)

const rrfConfig = {};

const RrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch
};

export function getReduxStore() {
  return store;
}

export function getRrfprop() {
  return RrfProps;
}

export default rootReducer