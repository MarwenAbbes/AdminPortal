import * as types from "./actionTypes"
import firebase from "./firebase"

const registerStart = () =>({
    type : types.REGISTER_START,
    
}
)

const registerSuccess = (user) => ({
    type : types.REGISTER_SUCCESS,
    payload : user
})

const registerFail = (error) => ({
    type : types.REGISTER_FAIL,
    payload : error
})


const loginStart = () =>({
    type : types.LOGIN_START,
    
}
)

const loginSuccess = (user) => ({
    type : types.LOGIN_SUCCESS,
    payload : user
})

const loginFail = (error) => ({
    type : types.LOGIN_FAIL,
    payload : error
})

const logoutStart = () =>({
    type : types.LOGOUT_START,
    
})

const logoutSuccess = (user) => ({
    type : types.LOGOUT_SUCCESS,
    payload : user
})

const logoutFail = (error) => ({
    type : types.LOGOUT_FAIL,
    payload : error
})

export const setUser = (user) => ({
    type : types.SET_USER,
    payload : user,
})

const googleSigninSuccess = (user) => ({
    type : types.GOOGLE_SIGN_IN_SUCCESS,
    payload : user
})

const  googleSigninFail = (error) => ({
    type : types.GOOGLE_SIGN_IN_FAIL,
    payload : error
})

const  googleSigninStart = () =>({
    type : types.GOOGLE_SIGN_IN_START,
    
})


export const registerInitiate = (email, password, displayName) =>{
    return function(dispatch) {
        dispatch(registerStart());
        
        firebase.auth.createUserWithEmailAndPassword(email,password).then(({user}) => {
            user.updateProfile({
                displayName
            })
            dispatch(registerSuccess(user))
           
        }).catch((err) => dispatch(registerFail(err.message)))
       
    }
} 

export const loginInitiate = (email, password) =>{
    return function(dispatch) {
        dispatch(loginStart());
        
       firebase.auth.signInWithEmailAndPassword(email,password).then(({user}) => {
            dispatch(loginSuccess(user))
           
        }).catch((err) => dispatch(loginFail(err.message)))
       
    }
} 

export const logoutInitiate = (email, password) =>{
    return function(dispatch) {
        dispatch(logoutStart());
        
       firebase.auth.signOut().then((resp) => 
            dispatch(logoutSuccess()))
            .catch((err) => dispatch(logoutFail(err.message)))
       
    }
} 


export const googleSigninInitiate = (email, password) =>{
    return function(dispatch) {
        dispatch(googleSigninStart());
        
       firebase.auth.signInWithPopup(firebase.googleAuthProvider).then(({user}) => {
            dispatch(googleSigninSuccess(user))
           
        }).catch((err) => dispatch(googleSigninFail(err.message)))
       
    }
} 

