import React, {useEffect} from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import {useDispatch} from 'react-redux'
import Login from './Pages/LoginPage/Login';
import Register from './Pages/SignupPage/Register';
import HomePage from './Pages/HomePage/HomePage';
import AdminPage from './Pages/AdminPage/AdminPage';
import ShowAllArticles from './Pages/BlogPages/ShowAllArticles/ShowAllArticles';
import ShowAllVideos from './Pages/VideoPages/ShowAllVideos/ShowAllVideos'
import FullArticleLayout from './Pages/BlogPages/FullArticleLayout/FullArticleLayout';
import EditArticles from './Pages/BlogPages/EditArticle/EditArticle';
import WorkShopPage from './Pages/WorkshopPage/WorkShopPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import {auth} from "./Config/firebase"
import {setUser} from './Config/actions'
import UserList from './Pages/UserList/UserList';
import {Provider} from 'react-redux'
import {getReduxStore, getRrfprop} from './Config/RootReducer'
import {ReactReduxFirebaseProvider} from "react-redux-firebase";
import RouterManager from './Components/UserRoute/RouterManager';

function App() {

    const dispatch = useDispatch();

    useEffect(() => {
        auth.onAuthStateChanged((authUser) => {
            if (authUser) {
                dispatch(setUser(authUser))
            } else {
                dispatch(setUser(null))
            }
        })
    }, [dispatch])

    return (


        <BrowserRouter>
            <div className="App">

                <Provider store={
                    getReduxStore()
                }>
                    <ReactReduxFirebaseProvider {...getRrfprop()}>
                       <Route>
                           <RouterManager/>
                       </Route>
                    </ReactReduxFirebaseProvider>

                </Provider>


            </div>
        </BrowserRouter>


    );
}

export default App;
