import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux"
import {useHistory, Link} from "react-router-dom"
import {logoutInitiate} from "../../Config/actions"
import {Button} from "react-bootstrap";
import Topbar from "../../Components/TopBar/Topbar"


const HomePage = () => {

    const {currentUser} = useSelector((state) => state.user)
    const dispatch = useDispatch();

    const handleAuth = () =>{
        if(currentUser){
            dispatch(logoutInitiate())
        }
    }
    console.log(currentUser)

    return (
        <div>
            <Topbar/>
            <h1>Home Page {currentUser.displayName}   </h1>
            <Button className="btn btn-danger" onClick={handleAuth}>
                Logout
            </Button>
        </div>
        
    )

    
}

export default HomePage
