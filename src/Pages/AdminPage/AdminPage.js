import React, {useEffect} from "react";
import Topbar from "../../Components/TopBar/Topbar"
import Sidebar from "../../Components/SideBar/SideBar";
import FeaturedInfo from "../../Components/FeaturedInfo/FeaturedInfo";
import Chart from "../../Components/Chart/Chart";
import LargeWidget from "../../Components/Widgets/LargeWidget/LargeWidget";
import SmallWidget from "../../Components/Widgets/SmallWidget/SmallWidget";
import "./AdminPage.css"

export default function AdminPage() {
   
    const data = [
        {
            month: 'January ',
            tu: 4000,
            au: 10,
            ut: 20
        },
        {
            month: 'February ',
            tu: 3000,
            au: 20,
            ut: 30
        },
        {
            month: 'March ',
            tu: 2000,
            au: 30,
            ut: 40
        },
        {
            month: 'April ',
            tu: 2780,
            au: 40,
            ut: 50
        }, {
            month: 'May ',
            tu: 1890,
            au: 50,
            ut: 60
        }, {
            month: 'June ',
            tu: 2390,
            au: 60,
            ut: 70
        }, {
            month: 'July  ',
            tu: 3490,
            au: 70,
            ut: 80
        }, {
            month: 'August  ',
            tu: 4990,
            au: 80,
            ut: 90
        }, {
            month: 'September  ',
            tu: 2560,
            au: 90,
            ut: 110
        }, {
            month: 'October  ',
            tu: 1860,
            au: 120,
            ut: 90
        }, {
            month: 'November  ',
            tu: 8000,
            au: 140,
            ut: 150
        }, {
            month: 'December  ',
            tu: 8600,
            au: 160,
            ut: 150
        },

    ];
    return (
        <div>
            <Topbar/>
            <div className="containerSidebar">
                <Sidebar/>
                <div className="others">
                    <FeaturedInfo/>
                    <div className="ChartsContainer">
                        <Chart data={data}
                            title="User analytics"
                            dataKeyY1="tu"
                            dataKeyX="month"
                            dataKeyY2="au"
                            dataKeyY3="ut"/>
                    </div>
                    <div className="homeWidgets">
                        <SmallWidget/>
                        <LargeWidget/>
                    </div>


                </div>
            </div>
        </div>
    )
}
