import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {useHistory, Link} from 'react-router-dom'
import {registerInitiate} from "../../Config/actions";


import './Register.css'
import {
    Col,
    Row,
    Container,
    Form,
    Button

} from "react-bootstrap";

import uiImg from '../../Images/undraw_Hello_re_3evm.svg'
import {Input} from "reactstrap";
const Register = () => {
    const [state, setState] = useState({displayName: "", email: "", password: "", passwordConfirm: ""})
    const {email, password, displayName, passwordConfirm} = state

    const {currentUser} = useSelector((state) => state.user)
    const history = useHistory();
    useEffect(() => {
        if (currentUser) {
            history.push('/')
        }
    }, [currentUser, history])


    const dispatch = useDispatch()

    const handleSubmit = (e) => {

        e.preventDefault();
        if (password !== passwordConfirm) {
            return;
        }
        dispatch(registerInitiate(email, password, displayName))

        setState({email: "", displayName: "", password: "", passwordConfirm: ""})
    }
    const handleChange = (e) => {
        let {name, value} = e.target
        setState({
            ...state,
            [name]: value
        })
    };

    return (

        <>
            <Container className="container">
                <Row>

                    <Col lg={4}
                        md={6}
                        sm={12}
                        className="text-center form-block">
                        <Form id="submit-form"
                            onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Input type="text" name="displayName" id="display_name" placeholder="Display name" className="form-control"
                                    onChange={handleChange}
                                    defaultValue
                                    ={displayName}
                                    required/>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Input type="email" name="email" id="input_email" placeholder="Email address"
                                    onChange={handleChange}
                                    defaultValue
                                    ={email}
                                    required/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Control type="password" name="password" id="input_password" placeholder="Password"
                                    onChange={handleChange}
                                    defaultValue
                                    ={password}
                                    required/>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Control type="password" name="passwordConfirm" id="confirm_input_password" placeholder="Confirm Password"
                                    onChange={handleChange}
                                    defaultValue
                                    ={passwordConfirm}
                                    required/>
                            </Form.Group>
                            <Button className="col-12 Login-btn" type="submit" form="submit-form" value="submit">
                                <i className="fas fa-sign-in-alt"></i>
                                Sign up with Email</Button>


                            <hr className="solid"/>
                            <p>Already have an account ?
                                <Link to="./Login">
                                    Login Here</Link>
                            </p>
                        </Form>
                    </Col>


                    <Col lg={8}
                        md={6}
                        sm={12}>
                        <img src={uiImg}
                            className="w-100"
                            alt=""/>
                    </Col>


                </Row>
            </Container>
        </>
    )
}


export default Register
