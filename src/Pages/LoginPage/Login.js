import React, {useState, useEffect,Component} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {useHistory, Link} from 'react-router-dom'
import { connect } from "react-redux";
import firebase from "../../Config/firebase";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import './Login.css'
import {
    Col,
    Row,
    Container,
    Form,
    Button
} from "react-bootstrap";
import loginIcon from '../../Images/user.svg'
import uiImg from '../../Images/undraw_authentication_fsn5.svg'


   
    
        const uiConfig = {
            signInFlow: "popup",
            signInSuccessUrl: "/",
            signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID]
          };

class Login extends Component {
    constructor(props){
        super(props);
        this.state={
                email : "",
                password : "",
                
        }
    };
   

    
  render(){
    return (

        <div>
            <Container className="container">
                <Row>
                    <Col lg={8}
                        md={6}
                        sm={12}>
                        <img src={uiImg}
                            className="w-100"
                            alt=""/>
                    </Col>

                    <Col lg={4}
                        md={6}
                        sm={12}
                        className="text-center form-block">
                        <Form 
                            className="form-signin"
                            id="submit-form">
                            <h1>Welcome back</h1>
                            <Form.Group className="mb-3">
                                <Form.Control type="email" name="email" id="email" placeholder="Email address"
                                    
                                    required/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Control type="password" name="password" id="password" placeholder="Password"
                                    
                                    
                                    required/>
                            </Form.Group>
                            <Button className="col-12 Login-btn"
                             value="submit" type="submit"
            
                            >
                                Sign in with Email</Button>
                            <hr class="solid"/>

                            <Button className=" mt-2 col-12  google-btn social-btn" type="button">
                                {/* <span>
                                    <i className="far fa-google-plus-g"></i>Sign in with Google+
                                </span> */}
                                 <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
                            </Button>

                            <hr className="solid"/>
                            <p>Don't have an account ?
                                <Link to="./Register">
                                    Register Here</Link>
                            </p>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    )

  }  
    

    
}
const enhance = connect(({ firebase: { auth, profile } }) => ({
    auth,
    profile
  }));
  

export default enhance(Login)
