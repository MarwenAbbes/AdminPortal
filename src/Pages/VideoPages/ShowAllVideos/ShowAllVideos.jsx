import React, {Component, useState} from 'react'
import * as firebase from '../../../Config/firebase'
import VideoCard from '../../../Components/VideoCard/VideoCard'
import Topbar from '../../../Components/TopBar/Topbar'
import Sidebar from '../../../Components/SideBar/SideBar'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label
} from 'reactstrap';
import "./ShowAllVideos.css"


class ShowAllVideos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            videos: [],
            OpenModalState: false,
            Video: {
                Title: "",
                Category: "",
                URL: "",
                CreationDate: new Date()
            }
        }
    }


    componentDidMount() {
        this.getMyVideos()
    }

    getMyVideos = () => {
        firebase.db.collection("Videos").limit(10).get().then((docs) => {
            if (!docs.empty) {
                let allVideos = [];
                docs.forEach((doc) => {
                    const videos = {
                        id: doc.id,
                        ...doc.data()
                    };
                    allVideos.push(videos);
                });

                this.setState({
                    videos: allVideos
                }, () => {
                    this.setState({isLoaded: true});
                });
            }
        });
    };

    onchangeVideoTitle = (value) => {
        this.setState({
            Video: {
                ...this.state.Video,
                Title: value
            }
        })
    }

    onchangeVideoURL = (value) => {
        this.setState({
            Video: {
                ...this.state.Video,
                URL: value
            }
        })
    }

    onchangeVideoCategoryL = (value) => {
        this.setState({
            Video: {
                ...this.state.Video,
                Category: value
            }
        })
    }

    submitVideo = (e) => {
        const Video = this.state.Video
        firebase.db.collection("Videos").add(Video).then(() => {
            this.setState({
                Video: {

                    Title: "",
                    Category: "",
                    CreationDate: new Date(),
                    URL: ""

                }

            })

        }).catch(e => {
            console.log(e)
        })

    }

    render() {
        return (
            <div>
                <Topbar/>

                <div className="containerSidebar">
                    <Sidebar/>
                    <div className="others videosdisplay">
                        {
                        this.state.isLoaded ? this.state.videos.map((video, index) => {


                            return (
                                <>
                                <div className="YoutubeContainer">
                                <VideoCard className="YoutubeVideo" key={index}
                                        data={video}/>
                                </div>
                                   

                                </>
                            )


                        }) : 'No Videos'
                    }
                        <div>

                            <Modal isOpen={
                                this.state.OpenModalState
                            }>
                                <ModalHeader>
                                   Add Youtube Video
                                </ModalHeader>
                                <ModalBody>
                                    <Label>Video title :</Label>
                                    <Input type="text"
                                        onChange={
                                            (e) => this.onchangeVideoTitle(e.target.value)
                                        }
                                        value={
                                            this.state.Video.Title
                                        }/>
                                    <Label>Video URL :</Label>
                                    <Input type="text"
                                        onChange={
                                            (e) => this.onchangeVideoURL(e.target.value)
                                        }
                                        value={
                                            this.state.Video.URL
                                        }/>
                                    <Label>Video category :</Label>
                                    <Input type="text"
                                        onChange={
                                            (e) => this.onchangeVideoCategoryL(e.target.value)
                                        }
                                        value={
                                            this.state.Video.Category
                                        }/>
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="danger"
                                        onClick={
                                            () => {
                                                this.setState({OpenModalState: false})
                                            }
                                    }>Close</Button>
                                    <Button color="danger" onClick= {(k) => this.submitVideo() }>Submit</Button>
                                </ModalFooter>
                            </Modal>
                        </div>


                    </div>

                    <div className="otherone">
                        <div className="buttonsSide">

                            <Fab variant="extended" id="addVideoBtn" aria-label="add" className="AddArticleFAB"
                                onClick={
                                    () => {
                                        this.setState({OpenModalState: true})
                                    }
                            }>
                                <AddIcon/>
                                Add Video
                            </Fab>
                        </div>
                    </div>
                </div>


            </div>


        );
    }


}
export default ShowAllVideos
