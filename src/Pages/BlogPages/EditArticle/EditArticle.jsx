import React, {Component} from 'react'
import classes from "./EditArticle.module.css"
import {withRouter} from 'react-router-dom';
import ReactQuill from 'react-quill';
import Topbar from '../../../Components/TopBar/Topbar';
import firebase from '../../../Config/firebase'
import "react-quill/dist/quill.snow.css";
import { ImageResize } from 'quill-image-resize';
import {
    Container,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Label,
    Input,
    Button
} from "reactstrap";
import Sidebar from '../../../Components/SideBar/SideBar';
import {v4 as uuidv4} from 'uuid'
import Snackbar from '@material-ui/core/Snackbar'
import Alert from 'react-bootstrap/Alert'


const db = firebase.firestore();
const storageRef = firebase.storage();
const formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "align",
  "strike",
  "script",
  "blockquote",
  "background",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "color",
  "code-block"
];

var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
];
const modules = {
  toolbar: {
    //container: "#toolbar",
    toolbar : toolbarOptions
    
  },
  history: {
    delay: 500,
    maxStack: 100,
    userOnly: true
  }
};



class EditArticles extends Component {
    constructor(props) {
        super(props);
        this.state = {
          article: {
            Title: "",
            Content: "",
            CreatedDate: new Date(),
            FeatureImage: "",
            IsPublished: false,
            LastModified: new Date(),
            CreatedUserID: ""
          },
          hasFeatureImage : false,
          snackbarOpen : false,
          snackbarMsg : ''
        };
      }
    
    
    
      onchangeArticleTitle = (value) => {
        this.setState({
          article: {
            ...this.state.article,
            Title: value
          }
        });
      };
    
      onChangeArticleContent = (value) => {
        this.setState({
          article: {
            ...this.state.article,
            Content: value
          }
        });
      };
    
      onChangePublish = (val) => {
        this.setState({
          article: {
            ...this.state.article,
            IsPublished: val === "True"
          }
        });
      };


     snackbarClose = (event) =>{
       this.setState({
         snackbarOpen : false
       })
     }

    
 submitArticle = () => {
   const article = this.state.article
   db.collection("Articles")
      .add(article)
      .then(() =>{
        this.setState({
          snackbarOpen : true,
          snackbarMsg : "Your article has been uploaded successfully",
          articles: {
            
            Title: "",
            Content: "",
            CreatedDate: new Date(),
            FeatureImage: "",
            IsPublished: false,
            LastModified: new Date(),
            CreatedUserID: ""
          },
          hasFeatureImage : false,
        })

      })
      .catch(e => {
        console.log(e)
      })

 }

 uploadImageCallBack = (e) =>{
   return new Promise(async(resolve, reject)=>{
     const file = e.target.files[0]
     const filename =  uuidv4()
     storageRef.ref().child("Articles/"+filename).put(file).then(async snapshot =>{
       const downloadURL = await storageRef.ref().child("Articles/"+filename).getDownloadURL()
       console.log(downloadURL)
       resolve({
         success: true,
         data : {link : downloadURL}
       })
     })
   })
 }

 


    render() {
        return (

            <div>
                <Topbar/>
                <div className="containerSidebar">
                    <Sidebar/>
                    <div className="others">
                    <Row>
          <Col xm={9} lg={9} md={8} sm={12} xs={12}>
            <Snackbar 
            anchorOrigin = {{vertical : 'bottom', horizontal : 'left' }}
            open = {this.state.snackbarOpen}
            autoHideDuration = {3000}
            onClose={this.snackbarClose} >
                <Alert
                onClose={this.snackbarClose} severity="success">
                {this.state.snackbarMsg}
                </Alert>
              </Snackbar>
            <h2 className={classes.SectionTitle}> New Article </h2>
            <FormGroup>
              <Label className={classes.Label}>Title </Label>
              <Input
                type="text"
                name="articleTitle"
                id="articleTitle"
                onChange={(e) => this.onchangeArticleTitle(e.target.value)}
                value={this.state.article.Title}
              />
            </FormGroup>

            
            <FormGroup>
              <div id="toolbar"/>

              <ReactQuill
                ref={(el) => (this.quill = el)}
                value={this.state.article.Content}
                onChange={(e) => this.onChangeArticleContent(e)}
                theme="snow"
                placeholder="Compose an epic..."
                modules={{
                  
                  toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block', 'image'],
                  
                    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                    [{ 'direction': 'rtl' }],                         // text direction
                  
                    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                  
                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],
                  
                    ['clean']                                         // remove formatting button
                  ]
              }}
                formats={formats}
              />

            
             </FormGroup>
           
          </Col>

          <Col xl={3} lg={3} md={4} ms={12} xs={12}>
            <Card>
              <CardHeader>Article settings</CardHeader>
              <CardBody>
                <FormGroup>
                  <Label className={classes.Label}> Publish </Label>
                  <Input
                    type="select"
                    name="publish"
                    id="publish"
                    onChange={(e) => this.onChangePublish(e.target.value)}
                  >
                    <option>False</option>
                    <option>True</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label className={classes.Label}> Feature image </Label>
                  <Input
                  type="file"
                  accept="image/*"
                  className={classes.ImageUploader}
                  onChange={async (e) => {
                    const uploadState = await this.uploadImageCallBack(e)
                    if(uploadState.success){
                      this.setState({
                        hasFeatureImage : true,
                        article : {
                            ...this.state.article,
                            FeatureImage : uploadState.data.link
                        }
                      })
                    }
                  }}
                  >
                    
                  </Input>
                  {
                    this.state.hasFeatureImage ? 
                    <img src={this.state.article.FeatureImage} className={classes.FeatureImg}/> : console.log(this.state.hasFeatureImage)
                  }
                </FormGroup>
                <FormGroup>
                  <Button
                    color="danger"
                    onClick={(e) => this.submitArticle()}
                  >
                    Submit
                  </Button>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>




                    </div>
                </div>
                
            </div>


        )
    }
}
export default EditArticles;
