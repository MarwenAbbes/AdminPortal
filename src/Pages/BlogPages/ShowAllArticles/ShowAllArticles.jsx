import React, {Component} from 'react'
import * as firebase from '../../../Config/firebase'
import ArticleCard from '../../../Components/ArticleCard/ArticleCard'
import Topbar from '../../../Components/TopBar/Topbar'
import Sidebar from '../../../Components/SideBar/SideBar'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import "./ShowAllArticles.css"
import {Link} from '@material-ui/core'


// const db = firebase.firestore();
class ShowAllArticles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            articles: []
        }
    }

    componentDidMount() {
        this.getMyArticles()
    }


    getMyArticles = () => {
        firebase.db.collection("Articles").limit(10).get().then((docs) => {
            if (!docs.empty) {
                let allArticles = [];
                docs.forEach((doc) => {
                    const article = {
                        id: doc.id,
                        ...doc.data()
                    };
                    allArticles.push(article);
                });

                this.setState({
                    articles: allArticles
                }, () => {
                    this.setState({isLoaded: true});
                });
            }
        });
    };


    render() {
        return (
            <div>
                <Topbar/>

                <div className="containerSidebar">
                    <Sidebar/>
                    <div className="others">
                        {
                        this.state.isLoaded ? this.state.articles.map((article, index) => {

                            if (article.IsPublished) {
                                return (
                                    <>

                                        <ArticleCard key={index}
                                            data={article}/>
                                    </>
                                )
                            }


                        }) : ''
                    } </div>
                    <div className="otherone">
                        <div className="buttonsSide">

                            <Fab href="/NewArticle" variant="extended" color="primary" aria-label="add" className="AddArticleFAB">
                                <AddIcon/>
                                New Article
                            </Fab>

                        </div>
                    </div>
                </div>


            </div>


        );
    }
}
export default ShowAllArticles;
