import React, {Componenet} from 'react'
import {withRouter} from 'react-router-dom';
import parse from 'html-react-parser'
import classes from './FullArticleLayout.module.css'
import {isLoaded} from 'react-redux-firebase';
import {Container} from "reactstrap";
import * as firebase from '../../../Config/firebase'
import Topbar from '../../../Components/TopBar/Topbar';
import Sidebar from '../../../Components/SideBar/SideBar';
import {
    EmailShareButton,
    FacebookShareButton,
    InstapaperShareButton,
    LinkedinShareButton,
    RedditShareButton,
    TelegramShareButton,
    TumblrShareButton,
    TwitterShareButton,
    VKShareButton,
    WhatsappShareButton,
    EmailIcon,
    FacebookIcon,
    FacebookMessengerIcon,
    InstapaperIcon,
    LinkedinIcon,
    RedditIcon,
    TelegramIcon,
    TumblrIcon,
    TwitterIcon,
    VKIcon,
    WhatsappIcon
} from "react-share";

// const db = firebase.firestore();

export function timeStampToString(ts) {
    const date = new Date(ts * 1000);
    return(date.getFullYear() + "/" + (
        date.getMonth() + 1
    ) + "/" + date.getDate());
}

const ShareUrl = window.location.href
class FullArticleLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            article: {},
            isLoaded: false
        }
    }

    
    componentDidMount() {
        if (typeof this.props.location.state !== 'undefind') {
            if (this.props.location.state.hasOwnProperty('article')) {
                this.setState({
                    article: this.props.location.state.article
                }, () => {
                    this.setState({isLoaded: true})
                })
            }
        }
    }
    render() {
        
        if (this.state.isLoaded) {
            return (
                <>
                    <Topbar/>
                    <div className="containerSidebar">
                        <Sidebar/>
                        <div className="others">

                            <div className={
                                classes.Article
                            }>
                                <div className={
                                    classes.ImageContainer
                                }>
                                    <img className={
                                            classes.Image
                                        }
                                        src={
                                            this.state.article.FeatureImage
                                        }
                                        alt={
                                            this.state.article.Title
                                        }/>
                                </div>

                                <div className={
                                    classes.ArticleInfomation
                                }>

                                    <h1 className={
                                        classes.Title
                                    }>
                                        {
                                        this.state.article.Title
                                    }</h1>

                                </div>
                                <div className={
                                    classes.Date
                                }>
                                    <div>
                                        Published by : {}
                                        | at {
                                        timeStampToString(this.state.article.LastModified.seconds)
                                    } </div>
                                </div>
                                    <br/>
                                <EmailShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <EmailIcon size={30} round={true}/>
                                </EmailShareButton>
                                <FacebookShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <FacebookIcon size={30} round={true}/>
                                </FacebookShareButton>
                               
                                <LinkedinShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <LinkedinIcon size={30} round={true}/>
                                </LinkedinShareButton>
                                <RedditShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <RedditIcon size={30} round={true}/>
                                </RedditShareButton>
                                <TelegramShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <TelegramIcon size={30} round={true}/>
                                </TelegramShareButton>


                                <TumblrShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <TumblrIcon size={30} round={true}/>
                                </TumblrShareButton>
                                <TwitterShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <TwitterIcon size={30} round={true}/>
                                </TwitterShareButton>
                                <VKShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <VKIcon size={30} round={true}/>
                                </VKShareButton>
                                <WhatsappShareButton className={classes.ShareBtn} url={ShareUrl}>
                                        <WhatsappIcon size={30} round={true}/>
                                </WhatsappShareButton>
                                <div className={
                                    classes.ArticleMain
                                }>
                                    {
                                    parse(this.state.article.Content)
                                } </div>
                            </div>

                        </div>
                    </div>


                </>
            )

        } else 
            return (
                <div>
                    Loading...
                </div>
            )


        


    }
}
export default withRouter(FullArticleLayout)
