import React from 'react'
import {DataGrid} from '@material-ui/data-grid';
import {DeleteOutline
} from '@material-ui/icons';
import Topbar from '../../Components/TopBar/Topbar'
import Sidebar from '../../Components/SideBar/SideBar'

import "./UserList.css"

export default function UserList() {

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            width: 90
        },
        {
            field: 'user',
            headerName: 'Full name',
            width: 150,
            editable: false,
            renderCell:(params) =>{
                return(
                    <div className="userListUser">
                        <img src={params.row.avatar} alt="" className="userListImg"/>
                        {params.row.user}
                    </div>
                )
            }
        },
        {
            field: 'age',
            headerName: 'Age',
            type: 'number',
            width: 110,
            editable: false
        }, 
        {
            field: 'title',
            headerName: 'Title',
            width: 150,
            editable: false
        }, 
        {
            field: 'etablisment',
            headerName: 'Etablissement',
            width: 150,
            editable: false
        }, 
        {
            field : 'email',
            headerName: "Email",
            width : 200,
            editable : false
        },
        {
            field : 'phone',
            headerName: "Phone number",
            width : 180,
            editable : false
        },
        {
            field : "actions",
            headerName : "Actions",
            width: 150,
            renderCell : (params) => {
                return(
                    <>
                    <button className="userListBtnDelete">Delete</button>   
                    </>
                )
            }
        }

    ];

    const rows = [
        {
            id: 1,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 35,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        },
        {
            id: 2,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 42,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
            
        },
        {
            id: 3,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 45,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        },
        {
            id: 4,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 16,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        }, {
            id: 5,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 21,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        }, {
            id: 6,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 18,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        }, {
            id: 7,
            avatar : "https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg",
            user: 'Snow Jon',
            age: 44,
            title : 'Student',
            etablisment : 'ISIMM',
            email : "marwen_solo@hotmail.com",
            phone: "50 258 245"
        }
    ];

    return (
        <div>
            <Topbar/>
            <div className="containerSidebar">
                <Sidebar/>
                <div className="userList">
                    <DataGrid rows={rows}
                        columns={columns}
                        pageSize={5}
                        checkboxSelection
                        disableSelectionOnClick/>
                </div>
            </div>
        </div>

    )
}
