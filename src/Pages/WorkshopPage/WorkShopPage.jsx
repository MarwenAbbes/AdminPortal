import React, {Component, useState} from 'react'
import Topbar from '../../Components/TopBar/Topbar';
import Sidebar from '../../Components/SideBar/SideBar';
import  firebase from '../../Config/firebase'
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import DocumentCard from '../../Components/DocumentCard/DocumentCard';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
    Label
} from 'reactstrap';
import {v4 as uuidv4} from 'uuid'
import "./WorkShopPage.css"


const db = firebase.firestore();
const storageRef = firebase.storage();
class WorkShopPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            documents: [],
            OpenModalState: false,
            Document: {
                Title: '',
                Category: '',
                URL: '',
                CreationDate: new Date()
            }
        }
    }


    componentDidMount() {
        this.getMyDocuments()
    }

    getMyDocuments = () => {
       db.collection("Documents").limit(10).get().then((docs) => {
            if (!docs.empty) {
                let allDocuments = [];
                docs.forEach((doc) => {
                    const documents = {
                        id: doc.id,
                        ...doc.data()
                    };
                    allDocuments.push(documents);
                });

                this.setState({
                    documents: allDocuments
                }, () => {
                    this.setState({isLoaded: true});
                });
            }
        });
    };

    onchangeDocumentTitle = (value) => {
        this.setState({
            Document: {
                ...this.state.Document,
                Title: value
            }
        })
    }

    onchangeDocumentCategoryL = (value) => {
        this.setState({
            Document: {
                ...this.state.Document,
                Category: value
            }
        })
    }

    submitDocument = (e) => {
        const Document = this.state.Document
        db.collection("Documents").add(Document).then(() => {
            this.setState({
                Document: {

                    Title: "",
                    Category: "",
                    CreationDate: new Date(),
                    URL: ""

                }

            })

        }).catch(e => {
            console.log(e)
        })

    }

    uploadDocumentCallBack = (e) => {
        return new Promise(async (resolve, reject) => {
            const file = e.target.files[0]
            const filename = uuidv4()
            storageRef.ref().child("Documents/" + filename).put(file).then(async snapshot => {
                const downloadURL = await storageRef.ref().child("Documents/" + filename).getDownloadURL()
                console.log(downloadURL)
                resolve({
                    success: true,
                    data: {
                        link: downloadURL
                    }
                })
            })
        })
    }

    render() {
        return (
            <>
                <Topbar/>
                <div className="containerSidebar">
                    <Sidebar/>


                    <div className="others documentDisplay">
                        {
                        this.state.isLoaded ? this.state.documents.map((document, index) => {
                            return (
                               
                                <div className="DocumentContainer">
                                    <DocumentCard key={index}
                                        data={document}/>
                                        </div>
                                
                            )


                        }) : ''
                    }
                        <Modal isOpen={
                            this.state.OpenModalState
                        }>
                            <ModalHeader>
                                Add Workshop Document
                            </ModalHeader>
                            <ModalBody>
                                <Label>Document title :</Label>
                                <Input type="text"
                                    onChange={
                                        (e) => this.onchangeDocumentTitle(e.target.value)
                                    }
                                    value={
                                        this.state.Document.Title
                                    }/>
                                <Label>Document :</Label>
                                <Input type="file" accept="application/pdf"
                                    onChange={
                                        async (e) => {
                                            const uploadState = await this.uploadDocumentCallBack(e)
                                            if (uploadState.success) {
                                                this.setState({
                                                    
                                                    Document: {
                                                        ...this.state.Document,
                                                        URL: uploadState.data.link
                                                    }
                                                })
                                            }
                                        }
                                } ></Input>
                            <Label>Document category :</Label>
                            <Input type="text"
                                onChange={
                                    (e) => this.onchangeDocumentCategoryL(e.target.value)
                                }
                                value={
                                    this.state.Document.Category
                                }/>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger"
                                onClick={
                                    () => {
                                        this.setState({OpenModalState: false})
                                    }
                            }>Close</Button>
                            <Button color="danger" onClick= {(k) => this.submitDocument() }>Submit</Button>
                        </ModalFooter>
                    </Modal>

                </div>
                <div className="otherone">
                    <div className="buttonsSide">

                        <Fab variant="extended" id="addVideoBtn" aria-label="add" className="AddArticleFAB"
                            onClick={
                                () => {
                                    this.setState({OpenModalState: true})
                                }
                        }>
                            <AddIcon/>
                            Add Document
                        </Fab>
                    </div>
                </div>
            </div>

        </>
        )
    }

}

export default WorkShopPage
