import React from 'react'
import {
    Card,
    CardImg,
    CardTitle,
    CardSubtitle,
    CardBody,
    Badge,
    Video,
} from 'reactstrap'

import "./DocumentCard.css"
import pdfImage from '../../Images/pdf.png'
import GetApp from '@material-ui/icons/GetApp';

const DocumentCard = (props) => {


    return(
        <Card className="DocumentCardCard">
          <CardTitle className="DocumentCardTitle">
              <h3>{ props.data.Title} </h3>
          </CardTitle>
          <CardBody>
              <img className="PDFImageClass" src={pdfImage} alt=""/>
          </CardBody>
          <CardSubtitle className="DocumentCardSubTitle">
             <a href={props.data.URL} download> <span  >Download Here <GetApp/> </span></a>
          </CardSubtitle>
        </Card>
    )
}

export default DocumentCard