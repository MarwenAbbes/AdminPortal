import {red, green, blue} from '@material-ui/core/colors';
import React, {PureComponent} from 'react'
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from 'recharts';


import "./Chart.css"

export default function Chart({
    title,
    data,
    dataKeyX,
    dataKeyY1,
    dataKeyY2,
    dataKeyY3
}) {


    return (

        <ResponsiveContainer aspect={
            4 / 1
        }>
            <LineChart data={data}>
                <XAxis dataKey={dataKeyX}
                    stroke="#5550bd"/>
                <Tooltip/>
                <CartesianGrid strokeDasharray="3 3" stroke="#e0dfdf"/>
                <Line type="monotone"
                    dataKey={dataKeyY1}
                    stroke={"#5550bd"}/>
               
            </LineChart>

        </ResponsiveContainer>

    )
}
