import React, {Component} from 'react'
import "./topBar.css"
import {NotificationsNone, Language, Settings, ExitToApp} from '@material-ui/icons';
import ProfileImg from "../../Images/ProfileImg.jpg"
import * as firebase from '../../Config/firebase'
import {connect} from "react-redux";
import {Button} from "react-bootstrap";


function menuToggle() {
    const toggleMenu = document.querySelector('.menu');
    toggleMenu.classList.toggle('active');
}

var user;
firebase.auth.currentUser ? user = firebase.auth.currentUser : user = ''

class Topbar extends Component {
    constructor(props) {
        super(props);
    }


    render() {
      
        return (


            <div className="topbar">
                <div className="topbarWrapper">
                    <div className="topleft">
                        <span className="logo">
                            DasEducation
                        </span>
                    </div>
                    <div className="topright">
                        <div className="topbarIconContainer">
                            <NotificationsNone/>
                            <span className="topIconbadge">2</span>
                        </div>

                        <div className="topbarIconContainer">
                            <Language/>
                        </div>

                        <div className="topbarIconContainer">
                            <Settings/>
                            <span className="topIconbadge">2</span>
                        </div>

                        <img src={ProfileImg}
                            alt=""
                            className="topAvatar"
                            onClick={menuToggle}/>

                        <div className="menu">
                            <h3>{
                                this.props.auth.isEmpty ?'': this.props.auth.displayName
                               
                            }
                                <br/>
                                <span>
                                    Software Engineer</span>
                            </h3>
                            <ul>
                                <li>
                                    <ExitToApp className="exitAppClass"/>
                                    {
                                        this.props.auth.isEmpty
                                        ? ''
                                        :
                                        <Button onClick={
                                            () => firebase.auth.signOut()
                                        }>Logout</Button>
                                    }
                                   
                                </li>
                            </ul>
                        </div>


                    </div>

                </div>


            </div>

        )
    }


}

const enhance = connect(({
    firebase: {
        auth,
        profile
    }
}) => ({auth, profile}));
export default enhance(Topbar)
