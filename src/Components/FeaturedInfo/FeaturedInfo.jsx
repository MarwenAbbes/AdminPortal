import React from 'react'
import {ArrowUpward} from '@material-ui/icons';
import "./FeaturedInfo.css"
import * as firebase from '../../Config/firebase'


class FeaturedInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ArticleNumber : 0,
            DocumentNumber : 0,
            VideoNumber : 0,
        }
    }

    
    render() {
        this.GetNumbers();
        return (
            <div>
                <div className="featured">
                    <div className="featuredItem Articles">
                        <span className="featuredTitle">Articles</span>
                        <div className="featuredMoneyContainer">
                            <span className="featuredMoney">
                                {
                                    this.state.ArticleNumber  
                                   
                                }
                            </span>
                            <span className="featuredobjectName">
                                Article</span>
                            <span className="featuredMoneyRate">+ 2</span>
                            <ArrowUpward className="featuredIcon"/>
                        </div>
                        <span className="featuredSub">
                            Compared to last week
                        </span>
                    </div>

                    <div className="featuredItem Documents">
                        <span className="featuredTitle">Documents</span>
                        <div className="featuredMoneyContainer">
                            <span className="featuredMoney">{this.state.DocumentNumber}</span>
                            <span className="featuredobjectName">
                                Document</span>
                            <span className="featuredMoneyRate">+ 6</span>
                            <ArrowUpward className="featuredIcon"/>
                        </div>
                        <span className="featuredSub">
                            Compared to last week
                        </span>
                    </div>

                    <div className="featuredItem Lessons">
                        <span className="featuredTitle">Lessons</span>
                        <div className="featuredMoneyContainer">
                            <span className="featuredMoney">{this.state.VideoNumber}</span>
                            <span className="featuredobjectName">
                                Lessons</span>
                            <span className="featuredMoneyRate">+ 10</span>
                            <ArrowUpward className="featuredIcon"/>
                        </div>
                        <span className="featuredSub">
                            Compared to last week
                        </span>
                    </div>
                </div>
            </div>
        )

       
    }

    GetNumbers() {
        firebase.db.collection("Articles").get().then((querySnapshot) => {
            this.setState({
                ArticleNumber: querySnapshot.size,
            });
        });
        firebase.db.collection("Documents").get().then((querySnapshot) => {
            this.setState({
                DocumentNumber: querySnapshot.size,
            });
        });
        firebase.db.collection("Videos").get().then((querySnapshot) => {
            this.setState({
                VideoNumber: querySnapshot.size,
            });
        });
    }
}
export default FeaturedInfo
