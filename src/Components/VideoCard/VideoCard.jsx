import React from 'react'
import {
    Card,
    CardImg,
    CardTitle,
    CardSubtitle,
    CardBody,
    Badge,
    Video,
} from 'reactstrap'
import classes from './VideoCard.css'
import {Link} from "react-router-dom";
import YouTube from 'react-youtube';




const VideoCard = (props) => {
   


    function _onReady(e) {
        // access to player in all event handlers via event.target
        e.target.pauseVideo();
      }
    const opts = {
       height: '390',
        width: '640',
        playerVars: {
         
          autoplay: 0,
        },}


      
    return (
         
          <Card >
    
                <YouTube 
                
                videoId={props.data.URL}
                opts={opts} 
                onReady={_onReady} />
 
        </Card>
          )


        

  
   
}
export default VideoCard