import React, {Component} from 'react'
import {BrowserRouter as Router,Route, Switch, withRouter} from 'react-router-dom'
import AdminPage from '../../Pages/AdminPage/AdminPage'
import UserList from '../../Pages/UserList/UserList';
import ShowAllVideos from '../../Pages/VideoPages/ShowAllVideos/ShowAllVideos';
import ShowAllArticles from '../../Pages/BlogPages/ShowAllArticles/ShowAllArticles';
import EditArticles from '../../Pages/BlogPages/EditArticle/EditArticle';
import WorkShopPage from '../../Pages/WorkshopPage/WorkShopPage';
import FullArticleLayout from '../../Pages/BlogPages/FullArticleLayout/FullArticleLayout';
import Login from '../../Pages/LoginPage/Login';
import Register from '../../Pages/SignupPage/Register';
import {connect} from 'react-redux'
import firebase from 'firebase/app'
import'firebase/auth'


const AdminOnly = (ComposedComponent, auth) => {

    class AdminOnly extends Component {
        constructor(props) {
            super(props);
            this.state = {
                isPassed: false
            }
        }

        componentWillMount() {
            
            if (!auth.isEmpty) {
                firebase.auth().currentUser.getIdTokenResult().then((idTokenResult) => {
                    if (idTokenResult.claims.type === 'administrator') {
                        this.setState({isPassed: true})
                        console.log('passed')
                    } else {
                        this.props.history.push('/login');
                        console.log('refused')
                    }
                })
            } else {
                this.props.history.push('/login')
            }
        }
        render() {

            console.log('hi')
            console.log(firebase.auth().token)
            if (this.state.isPassed) {
                return <ComposedComponent location={
                        this.props.location
                    }
                    history={
                        this.props.history
                    }
                    auth={
                        this.props.auth
                    }/>
            } else {
                <div>
                    Checking....
                </div>
            }


        }


    }
    return AdminOnly
}


class RouterManager extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        return (
            <div>
                <Switch>
                    <Route exact path="/">
                        <AdminPage/>
                    </Route>
                    <Route path="/users" component={AdminOnly(UserList, this.props.auth)}>
                      
                    </Route>
                    <Route path="/Videos">
                        <ShowAllVideos/>
                    </Route>
                    <Route path="/NewArticle">
                        <EditArticles/>
                    </Route>
                    <Route path="/Documents">
                        <WorkShopPage/>
                    </Route>
                    <Route path="/articles">
                        <ShowAllArticles/>
                    </Route>
                    <Route path="/article/:id">
                        <FullArticleLayout/>
                    </Route>
                    <Route path="/login">
                        <Login/>
                    </Route>
                    <Route path="/Register"><Register/></Route>

                </Switch>

            </div>
        )
    }

}
const enhance = connect(({
    firebase: {
        auth,
        profile
    }
}) => ({auth, profile}));
export default enhance(withRouter(RouterManager))
