import React from 'react'
import {Visibility} from '@material-ui/icons';
import "./SmallWidget.css"

export default function SmallWidget() {
    return (
        <div className="widgetSm">
            <span className="widgetSmallTitle">
                New Join Members</span>
            <ul className="widgetSmList">
                <li className="widgetSmallListItem">
                    <img src="https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg" alt="" className="widgetSmImg"/>
                    <div className="widgetSmUser">
                        <span className="widgetSmUserName">Anna Keller</span>
                        <span className="widgetSmUserCollege">ISIMM</span>
                    </div>
                    <button className="widgetSmButton">
                        <Visibility className="widgetSmIcon"/>
                        View
                    </button>
                </li>
                <li className="widgetSmallListItem">
                    <img src="https://image.freepik.com/photos-gratuite/sympathique-femme-souriante-recherche-plaisir-avant_176420-20779.jpg" alt="" className="widgetSmImg"/>
                    <div className="widgetSmUser">
                        <span className="widgetSmUserName">Anna Keller</span>
                        <span className="widgetSmUserCollege">ISIMM</span>
                    </div>
                    <button className="widgetSmButton">
                    <Visibility className="widgetSmIcon"/>
                    View
                    </button>
                </li>
                <li className="widgetSmallListItem">
                    <img src="https://image.freepik.com/photos-gratuite/sourire-confiant-femme-affaires-posant-bras-croises_1262-20950.jpg" alt="" className="widgetSmImg"/>
                    <div className="widgetSmUser">
                        <span className="widgetSmUserName">Anna Keller</span>
                        <span className="widgetSmUserCollege">ISIMM</span>
                    </div>
                    <button className="widgetSmButton">
                    <Visibility className="widgetSmIcon"/>
                    View
                    </button>
                </li>
                <li className="widgetSmallListItem">
                    <img src="https://image.freepik.com/photos-gratuite/portrait-homme-affaires-confiant-positif_1262-17122.jpg" alt="" className="widgetSmImg"/>
                    <div className="widgetSmUser">
                        <span className="widgetSmUserName">Anna Keller</span>
                        <span className="widgetSmUserCollege">ISIMM</span>
                    </div>
                    <button className="widgetSmButton">
                    <Visibility className="widgetSmIcon"/>
                    View
                    </button>
                </li>
            </ul>
        </div>
    )
}
