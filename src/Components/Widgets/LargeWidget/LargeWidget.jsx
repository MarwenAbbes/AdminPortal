import React from 'react'
import "./LargeWidget.css"


export default function LargeWidget() {

const Button = ({type}) =>{
    return <button className={"widgetLgButton " + type}>{type}</button>
}


    return (
        <div className="widgetLg">
            <h3 className="widgetLgTitle">Latest comments</h3>
            <table className="widgetLgTable">
                <tr className="widgetLgTr">
                    <th className="widgetLgTh">User</th>
                    <th className="widgetLgTh">Date</th>
                    <th className="widgetLgTh">Article</th>
                    <th className="widgetLgTh">Status</th>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/portrait-homme-blanc-isole_53876-40306.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Approved"/>
                    </td>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/sympathique-femme-souriante-recherche-plaisir-avant_176420-20779.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Declined"/>
                    </td>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/sourire-confiant-femme-affaires-posant-bras-croises_1262-20950.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Pending"/>
                    </td>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/portrait-homme-affaires-confiant-positif_1262-17122.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Approved"/>
                    </td>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/sourire-confiant-femme-affaires-posant-bras-croises_1262-20950.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Pending"/>
                    </td>
                </tr>
                <tr className="widgetLgTr">
                    <td className="widgetLgUser">
                        <img className="widgetLgImg" src="https://image.freepik.com/photos-gratuite/portrait-homme-affaires-confiant-positif_1262-17122.jpg" alt=""/>
                        <span className="widgetLgName">Anna Keller</span>
                    </td>
                    <td className="widgetLgDate">10 Sept 2021</td>
                    <td className="widgetLgArticle"> DAS-FICO</td>
                    <td className="widgetLgStatus">
                        <Button type="Approved"/>
                    </td>
                </tr>
            </table>
        </div>
    )
}
