import React from 'react'
import "./SideBar.css"
import {LineStyle,
     PermMedia,
      CardMembership,
      Comment,
    Description,
    Assessment,
    VideoLibrary,
    AttachFile,
    SupervisedUserCircle
} from '@material-ui/icons';

import { Link } from 'react-router-dom';

export default function Sidebar() {
    return (
        <div className="sidebar">
            <div className="sidebarWrapper">
                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">
                        Dashboard</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem active">
                            <LineStyle className="sidebarIcon"/>
                            <Link to="/" className="HypText">Home</Link>
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">
                    Media &#38; Documents</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <Description className="sidebarIcon"/>
                            <Link to ="/articles" className="HypText">Articles </Link>
                        </li>
                        <li className="sidebarListItem">
                            <AttachFile className="sidebarIcon"/>
                            <Link to='/Documents' className="HypText">
                                WorkShops
                            </Link>
                        </li>
                        <li className="sidebarListItem">
                            <VideoLibrary className="sidebarIcon"/>
                           <Link to ="/Videos" className="HypText"> Videos</Link>
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">
                    Certification</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <Assessment className="sidebarIcon"/>
                            Entry level test
                        </li>
                        <li className="sidebarListItem">
                            <CardMembership className="sidebarIcon"/>
                            Das certificate
                        </li>
                    </ul>
                </div>

                <div className="sidebarMenu">
                    <h3 className="sidebarTitle">
                    Users</h3>
                    <ul className="sidebarList">
                        <li className="sidebarListItem">
                            <SupervisedUserCircle className="sidebarIcon"/>
                            <Link to="/users" className="HypText"> Manage users </Link>
                            
                        </li>
                        <li className="sidebarListItem">
                            <Comment className="sidebarIcon"/>
                           Manage comments
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>
    )
}
